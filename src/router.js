import { OrderedMap, Iterable, JavaScript } from '@dragiyski/collection';

import Response from './response.js';

export default class Router {
    /**
     * @type {OrderedMap}
     */
    #routes = new OrderedMap({ indexing: false });

    prepend(id, definition, force = false) {
        validateRouteArguments(id, definition);
        if (!force && this.#routes.contains(id)) {
            throw new ReferenceError(`A route with id '${id}' is already defined`);
        }
        this.#routes.prepend(id, processDefinition(definition));
    }

    append(id, definition, force = false) {
        validateRouteArguments(id, definition);
        if (!force && this.#routes.contains(id)) {
            throw new ReferenceError(`A route with id '${id}' is already defined`);
        }
        this.#routes.append(id, processDefinition(definition));
    }

    contains(id) {
        return this.#routes.contains(id);
    }

    remove(id) {
        this.#routes.remove(id);
        return this;
    }

    async handle(request) {
        const queue = [];
        for (const [id, route] of this.#routes) {
            if (route.controller != null) {
                if (route.method != null) {
                    const routeMethod = route.method.toUpperCase();
                    const requestMethod = request.method.toUpperCase();
                    if (routeMethod !== requestMethod) {
                        continue;
                    }
                }
                let attributes;
                if (route.path != null) {
                    attributes = comparePath(route.path, request.path, route.namespace);
                    if (attributes == null) {
                        continue;
                    }
                } else if (route.match != null) {
                    attributes = compareMatch(route.match, request.path, route.namespace);
                    if (attributes == null) {
                        continue;
                    }
                } else {
                    attributes = Object.create(null);
                }
                attributes[Router.routeId] = id;
                if (typeof route.controller !== 'string' && typeof route.controller[Symbol.iterator] === 'function') {
                    let response = null;
                    for (const controller of route.controller) {
                        if (typeof controller !== 'function') {
                            continue;
                        }
                        const maybeResponse = await controller.call(attributes, request);
                        if (maybeResponse instanceof Response) {
                            response = maybeResponse;
                            break;
                        }
                        if (typeof maybeResponse === 'function') {
                            queue.push(maybeResponse);
                            continue;
                        }
                        if (maybeResponse != null) {
                            throw new TypeError(`Expected function specified as controller to return a function, a Response object or null/undefined, got ${JavaScript.getType(maybeResponse)}`);
                        }
                    }
                    if (response == null) {
                        continue;
                    }
                    while (queue.length > 0) {
                        const controller = queue.pop();
                        const maybeResponse = await controller.call(attributes, response);
                        if (maybeResponse instanceof Response) {
                            response = maybeResponse;
                            continue;
                        }
                        if (maybeResponse != null) {
                            throw new TypeError(`Expected function specified as controller delegate to return a Response object or null/undefined, got ${JavaScript.getType(maybeResponse)}`);
                        }
                    }
                    return response;
                } else if (typeof route.controller === 'function') {
                    const controller = route.controller;
                    const maybeResponse = await controller.call(attributes, request);
                    if (maybeResponse instanceof Response) {
                        let response = maybeResponse;
                        while (queue.length > 0) {
                            const controller = queue.pop();
                            const maybeNewResponse = await controller.call(attributes, response);
                            if (maybeNewResponse instanceof Response) {
                                response = maybeNewResponse;
                                continue;
                            }
                            if (maybeNewResponse != null) {
                                throw new TypeError(`Expected function specified as controller delegate to return a Response object or null/undefined, got ${JavaScript.getType(maybeResponse)}`);
                            }
                        }
                        return response;
                    } else if (typeof maybeResponse === 'function') {
                        queue.push(maybeResponse);
                    } else if (maybeResponse != null) {
                        throw new TypeError(`Expected function specified as controller to return a function, a Response object or null/undefined, got ${JavaScript.getType(maybeResponse)}`);
                    }
                }
            }
        }
        return new Response(404);
    }
}

Object.defineProperties(Router, {
    routeId: {
        value: Symbol('routeId')
    }
});

function processDefinition(definition) {
    definition = { ...definition };
    if (definition.path != null) {
        let path = definition.path;
        if (typeof path !== 'string' && typeof path[Symbol.iterator] === 'function') {
            path = [...Iterable(path).map(processPathEntry).flat().filter(entry => typeof entry === 'string' ? entry.length > 0 : true)];
        } else {
            path = processPathEntry(path).filter(entry => typeof entry === 'string' ? entry.length > 0 : true);
        }
        definition.path = path;
    } else if (definition.match != null) {
        let match = definition.match;
        if (typeof match !== 'string' && typeof match[Symbol.iterator] === 'function') {
            match = [...Iterable(match).map(processMatchEntry).filter(entry => typeof entry === 'string' ? entry.length > 0 : true)];
        } else {
            match = [processMatchEntry(match)].filter(entry => typeof entry === 'string' ? entry.length > 0 : true);
        }
        definition.match = match;
    }
    definition.namespace = !!definition.namespace;
    return definition;

    function processPathEntry(entry) {
        if (typeof entry === 'boolean') {
            entry = Number(entry);
        }
        if (typeof entry === 'number' || typeof entry === 'bigint') {
            entry = entry.toString(10);
        }
        if (typeof entry === 'string') {
            return entry.split('/');
        }
        if (typeof entry === 'object') {
            if (entry == null) {
                return [];
            }
            if (typeof entry.name !== 'string' && typeof entry.name !== 'symbol') {
                throw new TypeError(`Expected 'name' option of attribute object to be string/symbol`);
            }
            return entry;
        }
        throw new TypeError(`Route definition 'path' must be string|RegExp|Object(attribute) or Array<string|RegExp|Object(attribute)>`);
    }

    function processMatchEntry(entry) {
        if (typeof entry === 'boolean') {
            entry = Number(entry);
        }
        if (typeof entry === 'number' || typeof entry === 'bigint') {
            entry = entry.toString(10);
        }
        if (typeof entry === 'string') {
            return entry;
        }
        if (typeof entry === 'object') {
            if (entry == null) {
                return '';
            }
            if (typeof entry.name !== 'string' && typeof entry.name !== 'symbol') {
                throw new TypeError(`Expected 'name' option of attribute object to be string/symbol`);
            }
            return entry;
        }
        throw new TypeError(`Route definition 'match' must be string|RegExp|Object(attribute) or Array<string|RegExp|Object(attribute)>`);
    }
}

function validateRouteArguments(id, definition) {
    if (typeof id !== 'string') {
        throw new TypeError(`Expected argument 1 to be a string`);
    }
    if (definition == null || typeof definition !== 'object' && typeof definition !== 'function') {
        throw new TypeError(`Expected argument 2 to be an object or a function`);
    }
    if (definition.path != null && definition.match != null) {
        throw new TypeError(`Expected route definition to contain only one of the ["path", "match"] properties`);
    }
}

function comparePath(routePath, requestPath, namespace = false) {
    routePath = [...routePath];
    requestPath = [...Iterable(requestPath.split('/')).map(element => element.split('/')).flat().filter(element => element.length > 0)];
    const attributes = Object.create(null);
    while (routePath.length > 0 && requestPath.length > 0) {
        const routeEntry = routePath.shift();
        const requestEntry = requestPath.shift();
        if (typeof routeEntry === 'string') {
            if (routeEntry !== requestEntry) {
                return null;
            }
        } else if (routeEntry != null && typeof routeEntry === 'object') {
            let value;
            let match;
            if (typeof routeEntry.match === 'string') {
                if (routeEntry.match !== requestEntry) {
                    return null;
                }
                match = [value = requestEntry];
            } else if (routeEntry.match instanceof RegExp) {
                match = routeEntry.match.exec(requestEntry);
                if (match == null) {
                    return null;
                }
                value = match[0];
            } else {
                return null;
            }
            if (typeof routeEntry.toValue === 'function') {
                try {
                    value = routeEntry.toValue(...match);
                } catch (error) {
                    return null;
                }
            }
            if (routeEntry.name in attributes) {
                if (!Array.isArray(attributes[routeEntry.name])) {
                    attributes[routeEntry.name] = [attributes[routeEntry.name]];
                }
                attributes[routeEntry.name].push(value);
            } else if (routeEntry.array) {
                attributes[routeEntry.name] = [value];
            } else {
                attributes[routeEntry.name] = value;
            }
        }
    }
    if (routePath.length > 0) {
        return null;
    }
    if (requestPath.length > 0) {
        return namespace ? attributes : null;
    }
    return attributes;
}

function compareMatch(routeMatch, requestPath, namespace = false) {
    routeMatch = [...routeMatch];
    const attributes = Object.create(null);
    while (routeMatch.length > 0 && requestPath.length > 0) {
        const routeEntry = routeMatch.shift();
        if (typeof routeEntry === 'string') {
            if (!requestPath.startsWith(routeEntry)) {
                return null;
            }
            requestPath = requestPath.substr(routeEntry.length);
        } else if (routeEntry != null && typeof routeEntry === 'object') {
            let value;
            let match;
            if (typeof routeEntry.match === 'string') {
                if (!requestPath.startsWith(routeEntry.match)) {
                    return null;
                }
                match = [value = routeEntry.match];
                requestPath = requestPath.substr(routeEntry.match.length);
            } else if (routeEntry.match instanceof RegExp) {
                match = routeEntry.match.exec(requestPath);
                if (match == null) {
                    return null;
                }
                value = match[0];
                requestPath = requestPath.substr(match[0].length);
            } else {
                return null;
            }
            if (typeof routeEntry.toValue === 'function') {
                try {
                    value = routeEntry.toValue(...match);
                } catch (error) {
                    return null;
                }
            }
            if (routeEntry.name in attributes) {
                if (!Array.isArray(attributes[routeEntry.name])) {
                    attributes[routeEntry.name] = [attributes[routeEntry.name]];
                }
                attributes[routeEntry.name].push(value);
            } else if (routeEntry.array) {
                attributes[routeEntry.name] = [value];
            } else {
                attributes[routeEntry.name] = value;
            }
        }
    }
    if (routeMatch.length > 0) {
        return null;
    }
    if (requestPath.length > 0) {
        return namespace ? attributes : null;
    }
    return attributes;
}
