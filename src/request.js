import fs from 'fs/promises';
import net from 'net';
import path from 'path';
import multiparty from 'multiparty';
import { URL } from 'url';

import cookie from 'cookie';
import { Iterable } from '@dragiyski/collection';

import Response from './response.js';

const symbols = {
    request: Symbol('request')
};

export default class Request {
    constructor(request, options = {}) {
        options = { ...options };
        options.validateSNI ??= true;
        this[symbols.request] = request;
        this.httpVersion = request.httpVersion;
        this.httpVersionMajor = request.httpVersionMajor;
        this.httpVersionMinor = request.httpVersionMinor;
        this.headers = Object.assign(Object.create(null), request.headers);
        this.rawHeaders = [...request.rawHeaders];
        this.localAddress = Request.readAddress(request.socket.localAddress);
        this.remoteAddress = Request.readAddress(request.socket.remoteAddress);
        this.localPort = request.socket.localPort;
        this.remotePort = request.socket.remotePort;
        this.localFamily = net.isIP(request.socket.localAddress);
        this.remoteFamily = request.socket.remoteFamily;
        const location = new URL('https://localhost');
        if (request.authority) {
            location.host = request.authority;
        } else if (request.headers[':authority']) {
            location.host = request.headers[':authority'];
        } else if (request.headers.host) {
            location.host = request.headers.host;
        } else if (options.defaultHost != null) {
            location.host = options.defaultHost;
        } else {
            const error = new Response(400);
            error.message = `Unable to determine host: no HTTP/2 'authority', 'Host' header or 'defaultHost' option is specified`;
            error.name = 'BadRequestError';
            throw error;
        }
        if (request.socket.ssl != null) {
            this.ssl = request.socket.ssl;
            this.servername = request.socket.servername;
            if (options.validateSNI) {
                if (request.socket.servername && location.hostname !== request.socket.servername) {
                    const error = new Response(400);
                    error.message = 'SSL SNI does not match the HTTP host/authority';
                    error.name = 'BadRequestError';
                    throw error;
                }
            }
        }
        {
            let url = request.url.split('?');
            location.pathname = url.shift();
            url = url.join('?');
            location.search = url;
        }
        this.location = location;
        this.method = request.method;
        if (request.headers.accept) {
            this.accept = Request.parseHttpTokenHeaderWithProperties(request.headers.accept);
        } else {
            this.accept = Object.assign(Object.create(null), { '*/*': Object.create(null) });
        }
        if (request.headers['accept-encoding']) {
            this.acceptEncoding = Request.parseHttpTokenHeaderWithProperties(request.headers['accept-encoding']);
        } else {
            this.acceptEncoding = Object.create(null);
        }
        if (request.headers['accept-language']) {
            this.acceptLanguage = Request.parseHttpTokenHeaderWithProperties(request.headers['accept-language']);
        } else {
            this.acceptLanguage = Object.create(null);
        }
        if (request.headers['if-modified-since']) {
            this.ifModifiedSince = new Date(request.headers['if-modified-since']);
            if (isNaN(this.ifModifiedSince)) {
                this.ifModifiedSince = null;
            } else {
                this.ifModifiedSince.setMilliseconds(0);
            }
        } else {
            this.ifModifiedSince = null;
        }
        if (request.headers.cookie) {
            if (Array.isArray(request.headers.cookie)) {
                this.cookie = request.headers.cookie((target, headerLine) => {
                    return { ...target, ...cookie.parse(headerLine) };
                }, {});
            } else {
                this.cookie = cookie.parse(request.headers.cookie);
            }
            Object.setPrototypeOf(this.cookie, null);
        } else {
            this.cookie = Object.create(null);
        }
        if (typeof request.headers['content-type'] === 'string') {
            this.contentType = request.headers['content-type'].split(';')[0].trim().toLowerCase();
        }
        {
            const pathname = this.location.pathname.split('/');
            const relative = [];
            for (const entry of pathname) {
                if (entry === '..') {
                    relative.pop();
                } else if (entry !== '' && entry !== '.') {
                    relative.push(entry);
                }
            }
            this.path = relative.join('/');
        }
    }

    isMainRequest() {
        let hasSecFetchMeta = false;
        if ('sec-fetch-mode' in this.headers) {
            hasSecFetchMeta = true;
            if (['nested-navigate', 'navigate'].indexOf(this.headers['sec-fetch-mode']) < 0) {
                return false;
            }
        }
        if ('sec-fetch-dest' in this.headers) {
            hasSecFetchMeta = true;
            if (this.headers['sec-fetch-dest'] !== 'document') {
                return false;
            }
        }
        if (hasSecFetchMeta) {
            // At least one sec-fetch-* header was present and they pass the criteria.
            return true;
        }
        if (this.accept == null || !('index/html' in this.accept) && !('application/xhtml+xml' in this.accept)) {
            // All major browsers will send accept with at least text/html when the user perform initial navigation to the website.
            return false;
        }
        // noinspection RedundantIfStatementJS
        if ('x-requested-with' in this.headers && this.matchHeader('x-requested-with', 'xmlhttprequest', { caseInsensitive: true })) {
            // This is non-standard header, but it is sent sometimes, to indicate that the request is from XMLHttpRequest.
            // If send, it is not a browser navigation (even if it accept text/html).
            return false;
        }
        return true;
    }

    matchHeader(headerName, value, options) {
        headerName = headerName.toLowerCase();
        if (value === false && headerName in this.headers) {
            return true;
        }
        if (value === true && !(headerName in this.headers)) {
            return true;
        }
        if (Array.isArray(value)) {
            return value.some(value => this.matchHeader(headerName, value, options));
        } else if (typeof value !== 'string') {
            if (value != null && typeof value[Symbol.iterator] === 'function') {
                return Iterable(value).some(value => this.matchHeader(headerName, value, options));
            }
            return false;
        }
        if (headerName in this.headers) {
            const headerValue = this.headers[headerName];
            if (Array.isArray(headerValue)) {
                return headerValue.some(compareHaderValue);
            }
            if (typeof headerValue === 'string') {
                return compareHaderValue(headerValue);
            }
        }
        return false;

        function compareHaderValue(headerValue) {
            if (options?.caseInsensitive) {
                return headerValue.toLowerCase() === value.toLowerCase();
            }
            return value === headerValue;
        }
    }

    isAcceptable(mimeType) {
        mimeType = '' + mimeType;
        const acceptTypes = Object.keys(this.accept);
        if (acceptTypes.length <= 0) {
            // No 'Accept' header means we accept anything
            return true;
        }
        mimeType = mimeType.split('/');
        mimeLoop: for (let acceptType of Object.keys(this.accept)) {
            acceptType = acceptType.split('/');
            for (let i = 0; i < 2; ++i) {
                if (acceptType[i] !== '*' && acceptType[i] !== mimeType[0]) {
                    continue mimeLoop;
                }
            }
            return true;
        }
        return false;
    }

    static parseHttpTokenHeaderWithProperties(headerLine) {
        if (Array.isArray(headerLine)) {
            return headerLine.reduce((target, headerLine) => {
                const source = this.parseHttpTokenHeaderWithProperties(headerLine);
                for (const item in source) {
                    if (item in target) {
                        target[item] = { ...target[item], ...source[item] };
                        Object.setPrototypeOf(target[item], null);
                    } else {
                        target[item] = source[item];
                    }
                }
                return target;
            }, Object.create(null));
        }
        return Iterable(headerLine.split(','))
            .map(item => item.split(';'))
            .map(tokens => [
                tokens[0].trim(),
                Iterable(tokens.slice(1))
                    .map(token => token.split('='))
                    .map(kv => [kv[0], kv.slice(1).join('=')])
                    .reduce((target, kv) => {
                        target[kv[0].trim()] = kv[1].trim();
                        return target;
                    }, Object.create(null))
            ]).reduce((target, kv) => {
                const key = kv[0];
                const properties = kv[1];
                if (key in target) {
                    target[key] = { ...target.key, ...properties };
                    Object.setPrototypeOf(target[key], null);
                } else {
                    target[key] = properties;
                }
                return target;
            }, Object.create(null));
    }

    static async locateFilename(relative, directory, indexFiles) {
        if (directory != null && typeof directory !== 'string' && typeof directory[Symbol.iterator] === 'function') {
            for (const dir of directory) {
                const file = await this.locateFilename(relative, dir, indexFiles);
                if (file != null) {
                    return file;
                }
            }
            return null;
        }
        if (typeof directory !== 'string') {
            return null;
        }
        if (indexFiles == null) {
            indexFiles = [];
        } else if (typeof indexFiles === 'string') {
            indexFiles = [indexFiles];
        } else if (typeof indexFiles[Symbol.iterator] !== 'function') {
            indexFiles = [];
        }
        let filename = path.resolve(directory, relative);
        if (filename !== directory && !filename.startsWith(directory + path.sep)) {
            return null;
        }
        let filestat = null;
        try {
            filestat = await fs.stat(filename, { bigint: true });
        } catch {
            return null;
        }
        if (filestat.isDirectory()) {
            const directoryName = filename;
            filestat = null;
            for (const indexFile of indexFiles) {
                filename = path.resolve(directoryName, indexFile);
                try {
                    filestat = await fs.stat(filename, { bigint: true });
                } catch {
                    continue;
                }
                if (!filestat.isFile()) {
                    continue;
                }
                break;
            }
            if (filestat == null) {
                return null;
            }
        }
        if (!filestat.isFile()) {
            return null;
        }
        return {
            filename,
            stat: filestat
        };
    }

    async readBody() {
        return new Promise((resolve, reject) => {
            let buffer = Buffer.alloc(0);
            const request = this;
            this[symbols.request].once('error', onError).once('end', onEnd).on('data', onData).resume();

            function onData(data) {
                buffer = Buffer.concat([buffer, data]);
            }

            function onEnd() {
                this.off('error', onError).off('data', onData);
                resolve(buffer);
            }

            function onError(error) {
                this.off('end', onEnd).off('data', onData);
                reject(error);
            }
        });
    }

    async readJsonBody() {
        return JSON.parse(await this.readBody());
    }

    async readUrlEncodedBody() {
        return new URLSearchParams((await this.readBody()).toString('utf-8'));
    }

    async readMultipartBody() {
        return new Promise((resolve, reject) => {
            const form = new multiparty.Form({
                maxFieldsSize: 10 * 1024 * 1024,
                maxFilesSize: 100 * 1024 * 1024,
                maxFields: 1000
            });
            const result = Object.create(null);
            form.on('part', onPart).once('error', onError).once('close', onClose).parse(this[symbols.request]);

            function onPart(part) {
                let buffer = Buffer.alloc(0);
                part.on('data', onData).once('end', onEnd).once('error', onError);

                function onData(data, encoding) {
                    buffer = Buffer.concat([buffer, Buffer.from(data, encoding)]);
                }

                function onEnd() {
                    result[part.name] = {
                        headers: { ...part.headers },
                        filename: part.filename,
                        data: buffer,
                        offset: part.byteOffset
                    };
                    part.off('data', onData).off('error', onError);
                }

                function onError() {
                    part.off('data', onData).off('end', onEnd);
                }
            }

            function onError(error) {
                form.off('part', onPart).off('close', onClose);
                if (typeof error.statusCode === 'number') {
                    return void reject(new Response(error.statusCode, {
                        'Content-Type': 'text/plain;charset=utf-8'
                    }, error.stack));
                }
                reject(error);
            }

            function onClose() {
                form.off('part', onPart).off('error', onError);
                resolve(result);
            }
        });
    }

    static readAddress(source) {
        if (typeof source !== 'string') {
            return null;
        }
        const address = parseIPv6(source);
        if (address != null) {
            if (address[0] === 0 && address[1] === 0 && address[2] === 0 && address[3] === 0 && address[4] === 0 && address[5] === 65535) {
                // IP is actually IPv4 encoded as IPv6, so we extract it
                return (address[6] >> 8) + '.' + (address[6] & 0xFF) + '.' + (address[7] >> 8) + '.' + (address[7] & 0xFF);
            }
        }
        return source;
    }
}

function parseIPv6(ip) {
    ip = ip.split('::');
    let g = 0;
    for (let i = 0; i < ip.length; ++i) {
        ip[i] = ip[i].split(':');
        g += ip[i].length;
    }
    const ip4 = /([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/.exec(ip[ip.length - 1][ip[ip.length - 1].length - 1]);
    if (ip4) {
        ip[ip.length - 1].pop();
        ip[ip.length - 1].push(ip4[1] * 256 + (ip4[2] | 0));
        ip[ip.length - 1].push(ip4[3] * 256 + (ip4[4] | 0));
    }
    if (ip.length === 2 && g < 8) {
        if (ip[0].length === 1 && ip[0][0] === '') {
            ip[0].length = 0;
        }
        ip[2] = ip[1];
        ip[1] = [];
        while (g < 8) {
            ip[1].push(0);
            ++g;
        }
    }
    ip = Array.prototype.concat.apply([], ip);
    if (ip.length !== 8) {
        return null;
    }
    for (let i = 0; i < ip.length; ++i) {
        ip[i] = typeof ip[i] === 'number' ? ip[i] : ip[i].length > 0 ? parseInt(ip[i], 16) : 0;
        if (!isFinite(ip[i])) {
            return null;
        }
    }
    return ip;
}
